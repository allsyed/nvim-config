local cmd = vim.cmd -- to execute Vim commands e.g. cmd('pwd')
local fn = vim.fn -- to call Vim functions e.g. fn.bufnr()
local g = vim.g -- a table to access global variables
local opt = vim.opt -- to set options.

-- disable netrw, so we can use nvim-tree 
g.loaded_netrw = 1
g.loaded_netrwPlugin = 1


-- git clone https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim
require("package_manager")
require("settings")
require("key_map")
require("auto_cmd")

-- plugins
--
require("plugins")

cmd("colorscheme tokyonight")

-- Providers
--
g.loaded_ruby_provider = 0
g.loaded_perl_provider = 0
g.python3_host_prog = "$HOME/.envs/nvim_py3/bin/python"
-- g.python_host_prog = "$HOME/.envs/nvim_py2/bin/python"
g.python_host_prog = 0

-- Plugins settings
-- Projects
--[[ g.nvim_tree_update_cwd = 1
g.nvim_tree_respect_buf_cwd = 1 ]]
-- require('telescope').load_extension('projects')
-- require("telescope").load_extension("session-lens")
--
-- nvim-tree [https://github.com/kyazdani42/nvim-tree.lua]
--
-- closes the tree when you open a file
--g.nvim_tree_quit_on_open = 1
-- 0 by default, allows the cursor to be updated when entering a buffer
-- g.nvim_tree_follow = 1
-- 0 by default, update the path of the current dir if the file is not inside the tree.
-- g.nvim_tree_follow_update_path = 1
-- 0 by default, shows indent markers when folders are open
-- g.nvim_tree_indent_markers = 1

--
-- theme
--
vim.g.tokyonight_style = "night" -- day,night, storm
-- vim.g.tokyonight_transparent == true
-- vim.g.tokyonight_transparent_sidebar = true
