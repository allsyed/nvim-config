-- https://neovim.io/doc/user/lua.html
local api = vim.cmd

-- HTML
-- api([[
-- augroup html
-- autocmd FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2
-- augroup END
-- ]])


-- HTML django
api([[
augroup html_django
autocmd!
autocmd FileType htmldjango setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType htmldjango inoremap {{ {{  }}<left><left><left>
autocmd FileType htmldjango inoremap {% {%  %}<left><left><left>
autocmd FileType htmldjango inoremap {# {#  #}<left><left><left>
augroup END
]])

-- Restore previous position and center it on screen
api([[
autocmd BufReadPost * if line("'\"") > 0 && line ("'\"") <= line("$") | exe "normal! g'\"" | endif
autocmd BufRead * normal zz
]])

--source init.lua when saved
api([[
autocmd BufWritePost $MYVIMRC source $MYVIMRC
]])

-- Disable paste mode when leaving Insert Mode
api([[
au InsertLeave * set nopaste
]])

-- Set markdown type for different extensions
api([[
augroup set_markdown_ft
    autocmd!
    autocmd BufNewFile,BufFilePre,BufRead *.markdown,*.mdown,*.mkd,*.mkdn,*.mdwn,*.md,*.MD  set ft=markdown
augroup end
]])


-- Highlight yanked text
api([[
augroup highlightYank
    autocmd!
    autocmd TextYankPost * silent! lua vim.highlight.on_yank{higroup = "Visual", timeout = 300}
augroup END
]])
