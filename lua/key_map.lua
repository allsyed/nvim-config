local map = vim.api.nvim_set_keymap
no_remap = { noremap = true }
silent = { silent = true }
silent_no_remap = { noremap = true, silent = true }

-- map the leader key
map('n', '<20>', '', {})
vim.g.mapleader = ' '  -- 'vim.g' sets global variables

map("i", "fj", "<ESC>", {})
map("n", ";", ":", no_remap)
map("n", "<F2>", ":NvimTreeToggle<CR>", no_remap)
map("n", "<F3>", ":setlocal hlsearch!<CR>", no_remap)
map("n", "<F4>", ":setlocal spell!<CR>", no_remap)
map("n", "<F6>", ":set pastetoggle<CR>", no_remap)
map("n", "<C-w>", ":BufferClose<CR>", no_remap)
---- Select all text
map("n", "<Leader>a", "gg^VG", no_remap)
---- Cycle through tabs with <tab>
map("n", "<tab>", ":BufferNext<CR>", no_remap)
map("n", "<s-tab>", ":BufferPrevious<CR>", no_remap)


-- Save as sudo
map("c", "w!!", "w !sudo tee % >/dev/null", {})

---- move around windows

map("n", "<up>", "<C-w>k", no_remap)
map("n", "<down>", "<C-w>j", no_remap)
map("n", "<left>", "<C-w>h", no_remap)
map("n", "<right>", "<C-w>l", no_remap)
map("n", "<Leader>e", ":e $MYVIMRC<CR>", {})
map("n", "<Leader>so", ":source $MYVIMRC<CR>", {})

-- Searching
map("n", "<Leader>f", ":Telescope find_files<CR>", silent_no_remap)
map("n", "<Leader>rg", ":Telescope live_grep<CR>", silent_no_remap)
map("n", "<Leader>b", ":Telescope buffers<CR>", silent_no_remap)
map("n", "<C-B>", ":Telescope buffers<CR>", silent_no_remap)
map("n", "<Leader>ad", ":Telescope lsp_workspace_diagnostics<CR>", silent_no_remap)
map("n", "<F7>", ":Telescope session-lens search_session<CR>", silent_no_remap)

-- Code editing
-- Indentation
map("v", ">", ">gv", no_remap)
map("v", "<", "<gv", no_remap)
-- Format
map("n", "<F6>", [[ggvG$=<C-o>]], no_remap)

-- Case-conversion tools
-- snake_case -> camelCase
map("n", "<Leader>cc", "viw<Leader>cc", silent)
map("v", "<Leader>cc", [[:s/\%V_\(.\)/\U\1/g<CR>]], silent_no_remap)

-- snake_case -> PascalCase
-- snake_case -> PascalCase
map("n", "<Leader>pc", "viw<Leader>pc", silent)
map("v", "<Leader>pc", "<Leader>cc`<vU", silent)

-- camelCase/PascalCase -> snake_case
map("n", "<Leader>sc", "viw<Leader>sc", silent)
map("v", "<Leader>sc", [[:s/\%V\(\l\)\(\u\)/\1_\l\2/g<CR>`<vu]], silent_no_remap)

-- Telescope mappings
map('n', '<C-P>', [[:lua require'telescope'.extensions.project.project{display_type = "full"}<CR>]], no_remap)
map('n', '<Leader>b', [[:Telescope git_branches<CR>]], silent_no_remap)
map('n', '<Leader>g', [[:Telescope registers<CR>]], silent_no_remap)
map('n', '<F12>', [[:Telescope lsp_definitions<CR>zz]], silent_no_remap)
