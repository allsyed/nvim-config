--
-- Lualine [http://neovimcraft.com/plugin/hoob3rt/lualine.nvim/index.html]
--
return require("lualine").setup({
	extensions = { "nvim-tree", "quickfix" },
	options = {
		theme = "tokyonight",
	},
})
