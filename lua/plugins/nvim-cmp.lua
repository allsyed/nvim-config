--
-- nvim-cmp setup
--
vim.opt.completeopt = 'menu,menuone,noselect'
-- Uncomment below lines when you are going to include spell source
-- vim.opt.spell = true
-- vim.opt.spelllang = { 'en_us' }
local has_words_before = function()
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local cmp = require('cmp')
local luasnip = require('luasnip')
require("luasnip/loaders/from_vscode").lazy_load()


local cmp_kinds = {
    Class = ' ',
    Color = " ",
    Constant = 'ﲀ ',
    Constructor = " ",
    Constructor = ' ',
    EnumMember = ' ',
    Enum = '練',
    Event = ' ',
    Field = "",
    Field = ' ',
    File = '',
    Folder = "",
    Folder = ' ',
    Function = ' ',
    Interface = 'ﰮ ',
    Keyword = ' ',
    Method = ' ',
    Module = " ",
    Operator = "",
    Property = "ﰠ",
    Reference = ' ',
    Snippet = ' ',
    Struct = " ",
    Text = "",
    TypeParameter = ' ',
    Unit = '塞',
    Value = ' ',
    Variable = "",
}

cmp.setup {
    snippet = {
        expand = function(args)
            require('luasnip').lsp_expand(args.body)
        end,
    },
    mapping = {
        ["<C-p>"] = cmp.mapping.select_prev_item(),
        ["<C-n>"] = cmp.mapping.select_next_item(),
        ["<C-d>"] = cmp.mapping.scroll_docs(-4),
        ["<C-f>"] = cmp.mapping.scroll_docs(4),
        ["<C-Space>"] = cmp.mapping.complete(),
        ["<C-e>"] = cmp.mapping.close(),
        ["<CR>"] = cmp.mapping.confirm({
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
        }),
        ['<Tab>'] = function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            elseif has_words_before() then
                cmp.complete()
            else
                fallback()
            end
        end,
        ['<S-Tab>'] = function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
                luasnip.jump(-1)
            else
                fallback()
            end
        end,
    },
    experimental = {
        ghost_text = true,
        native_menu = false
    },

    formatting =  {
        format = function(entry, vim_item)
            vim_item.kind = string.format('%s %s', cmp_kinds[vim_item.kind], vim_item.kind)
            vim_item.menu = ({
                nvim_lsp = '[LSP]',
                luasnip = '[SNIP]',
                buffer = '[BUF]',
                nvim_lua = '[LUA]',
                path = '[PATH]',
                calc = '[CALC]',
                emoji = '[EMOJI]',
                rg = '[RipGrep]',
            })[entry.source.name]

            return vim_item
        end,
    },
    sources = {
        { name = 'nvim_lsp', priority = 11 },
        -- { name = 'spell' },
        { name = 'nvim_lua', priority = 1},
        { name = 'luasnip', priority = 2 },
        { name = 'buffer' , keyword_length = 4, priority = 10},
        { name = 'cmdline', priority = 91 },
        { name = 'path', priority = 90},
        -- { name = 'rg', priority = 20 },
    },
}
