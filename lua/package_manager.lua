local fn = vim.fn

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
    -- Highlight
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
    },
    {
        "nvim-telescope/telescope.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-telescope/telescope-project.nvim",
        },
    },
    "lewis6991/gitsigns.nvim",
    { 'TimUntersberger/neogit', dependencies = 'nvim-lua/plenary.nvim' },
    "kyazdani42/nvim-web-devicons",
    {
        "hoob3rt/lualine.nvim",
        dependencies = { "kyazdani42/nvim-web-devicons", lazy = true },
    },
    {
        "kyazdani42/nvim-tree.lua",
        dependencies = "kyazdani42/nvim-web-devicons"
    },
    "neovim/nvim-lspconfig",
    { "L3MON4D3/luasnip", dependencies = { "rafamadriz/friendly-snippets" }},
    {
        "hrsh7th/nvim-cmp",
        dependencies = {
            "saadparwaiz1/cmp_luasnip",
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-nvim-lua",
            "lukas-reineke/cmp-rg",
            -- "f3fora/cmp-spell",
            "hrsh7th/cmp-cmdline",
        },
    },
    {
        "romgrk/barbar.nvim",
        dependencies = { "kyazdani42/nvim-web-devicons" },
    },
    "numToStr/Comment.nvim",
    {
        "danymat/neogen",
        dependencies = "nvim-treesitter/nvim-treesitter",
        -- Uncomment next line if you want to follow only stable versions
        -- tag = "*"
    },
    -- Themes
    "folke/tokyonight.nvim",
    -- Indent lines
    "lukas-reineke/indent-blankline.nvim",
    {
        "jose-elias-alvarez/null-ls.nvim",
        dependencies = { "nvim-lua/plenary.nvim" },
    },
    "rcarriga/nvim-notify",
}
local opts = {}
require("lazy").setup(plugins, opts)
