local o = vim.o
local g = vim.g
local opt = vim.opt
local wo = vim.wo
local bo = vim.bo
local cmd = vim.cmd
local u = require("utils")

-- Global Options
opt.termguicolors = true
opt.laststatus = 2
opt.hidden = true
opt.number = true
opt.cursorline = true
-- opt.showcmd = true
opt.autoindent = true
opt.copyindent = true
opt.clipboard = "unnamedplus"
-- cmd([[set clipboard+=unnamedplus]])
--
-- Auto change directory is causing lots of problems.
-- opt.autochdir = true
opt.showmode = true

-- UI
opt.spell = true
opt.showmatch = true
opt.wrap = false
-- opt.wildmode = {"list:longest", "list:full"}
-- opt.list = true
opt.listchars = { eol = "↲", tab = "▸ ", trail = "·" }
--opt.mouse = "a"
cmd([[set mouse+=a]])

-- Search options
opt.hlsearch = true
opt.incsearch = true
opt.ignorecase = true

-- Tabs-spaces
opt.tabstop = 4
opt.softtabstop = 4
opt.expandtab = true
opt.shiftwidth = 4
opt.smarttab = true

-- Performance
o.sessionoptions = "blank,buffers,curdir,folds,help,options,tabpages,winsize,resize,winpos,terminal"
opt.swapfile = false
opt.backup = false
opt.autoread = true
opt.visualbell = false
opt.errorbells = false
opt.history = 1000
opt.undolevels = 1000
opt.wildignore = { "*.pyc" }
-- Performance : Disable some unused built-in plugins
g.loaded_gzip = false
g.loaded_netrwPlugin = false
g.loaded_tarPlugin = false
g.loaded_zipPlugin = false
g.loaded_2html_plugin = false
g.loaded_remote_plugins = false
